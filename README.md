# RED BLACK - BINARY SEARCH TREE
**Author:** Béuren Felipe Bechlin

Project developed in c language, available in GPL v3.
Implementation based on book *Introduction to Algorithms-3rd edition, Thomas H. Coremn*.

---

## Basic theory
Red black tree is a binary tree that follows rules to hold some balance
properties. The most important constraint is that any simple path from the root
to a leaf can't be more longer than twice times the shorter path.

The tree nodes have a extra field color that can be RED or BLACK and parent
pointer, compared with a normal *BST*.
**Red-Black properties:**
1. Every node is either red or black.
2. The root is black.
3. Every leaf(NIL) is black.
4. If a node is red, then both its children are black.
5. For each node, all simple paths from the node to descent leaves contain the
same number of black nodes.  

---

## Implementation
### Features
* Possibility to store any type of data in nodes.
* Possibility to chose the model of storage data in the node tree.
* Keys *integers* to order the tree nodes, that can be easily changed to
float for example.  
* Encapsulation about nodes and tree pointers.

### Usage
This project was developed in C language. To compile was used the *GCC*
compiler and only standard libraries available in the language.
A [Makefile](https://www.gnu.org/software/make/manual/make.html#Overview)
was wrote to compile, link and create the executable.
If you are in a Unix environment can execute:

``` Unix
make
```

So will be generated the executable __example_red_black_bst__.
To run:
``` Unix
./example_red_black_bst
```
### Data structure

To create a tree it's necessary create a pointer with type __RB_BST_TREE__.

``` c
RB_BST_TREE* myTree;
```

To store informations in nodes it's necessary to define some data structure or
use one of standard types variables. In the tree is only saved the pointer to
this data.

### Interface functions

List of functions that manipulate the tree:
* **rb_init_tree:** create e allocate memory to a new tree.
Furthermore select the info store mode.
* **rb_insert:** insert a node in the tree.
* **rb_search:** search for a node in the tree and return the data stored in him.
* **rb_delete:** delete a node if it exists.
* **rb_destroy_tree:** deallocate memory used in the tree.

---

### rb_init_tree
``` c
RB_BST_TREE* rb_init_tree(int info_size, bool info_copy);
```
**Arguments:**
1. **info_size:** size of the data that will be saved in nodes.
2. **info_copy:** mode that this data will be saved.
    * *true:* the field *info* when call insertion is copied and allocated
    in the heap, in others words, create a new pointer with this data.
    * *false:* only save the data pointer that was send in insertion
    routine, use this when you are secure that these data are saved in the memory.

**Return:**

Return a instance pointer of this tree.

``` c
myTree = rb_init_tree(sizeof(DATA_TEST), true);
```

### rb_insert
``` c
bool rb_insert(RB_BST_TREE* self, int key, void* info);
```
**Arguments:**
1. **self:** instance pointer of the tree.
2. **key:** key number of this new node, that's used to order the tree.
3. **info:** pointer to data that will be associated with this new node.

**Return:**

Return *true* if the operation was successful and *false* if not, remembering
that BST don't supports node with same key it's is processed like a fail.

``` c
printf("Key 10 was inserted with %s.\n",
    rb_insert(myTree, 10, &data_test_inst)? "success": "fail");
```

### rb_search
``` c
void* rb_search(RB_BST_TREE* self, int key);
```
**Arguments:**
1. **self:** instance pointer of the tree.
2. **key:** key number of the node to search.

**Return:**

Return a pointer to data that are store in the node or NULL with this key there
aren't in the tree. This operation depends of *info_copy* parameter that was
defined in the *rb_init_tree*.

**BE CAREFUL:** if the data in this pointer is modified it implies will be modified
in the tree too.

``` c
DATA_TEST* info;
info = (DATA_TEST *)rb_search(myTree, 10);
printf("KEY(10) was %s .\n", key[0], info != NULL ?"found":"not found");
```

If you want a local copy of the info to modified, can do this using *stdlib.h*:

``` c
DATA_TEST local_copy;
memcpy(&local_copy, rb_search(myTree, 10), sizeof(DATA_TEST));
```

### rb_delete
``` c
bool rb_delete(RB_BST_TREE* self, int key);
```
**Arguments:**
1. **self:** instance pointer of the tree.
2. **key:** key number of the node to delete.

**Return:**

Return *true* if the operation was successful and *false* if don't found this node.

``` c
printf("Key 10 was deleted with %s.\n",
    rb_delete(myTree, 10)? "success": "fail");
```

### rb_print_tree
``` c
void rb_print_tree(RB_BST_TREE* self);
```
**Arguments:**
1. **self:** instance pointer of the tree.

**Return:**

Return none.

``` c
rb_print_tree(myTree);
```

### rb_destroy_tree
``` c
void rb_destroy_tree(RB_BST_TREE* self);
```
**Arguments:**
1. **self:** instance pointer of the tree.

**Return:**

Return none.

``` c
rb_destroy_tree(myTree);
```
