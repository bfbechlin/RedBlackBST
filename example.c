#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "red_black_tree.h"

typedef struct _tst{
    int tcb;
    int ticket;
}TEST;
#define ELEMENTS 12
int main(){
    RB_BST_TREE* myTree;
    TEST tst;
    TEST *aux;
    int i, key[ELEMENTS];

    tst.tcb = ELEMENTS*2;
    tst.ticket = ELEMENTS;
    srand(time(NULL));

    // ***Initialing tree***
    // Arguments:
    // 1- int info_size:
    //      size of struct that will be saved as info on each node
    // 2- bool info_copy:(include <stdbool>)
    //      true: the data in the struct send when insert a node
    //          will be copied to a new variable with malloc
    //      false: only save the struct pointer send when insert a node
    // Return:
    //      RB_BST_TREE* self: instance of the tree, it's will be used
    //          in all interface functions
    myTree = rb_init_tree(sizeof(TEST), true);

    key[0] = rand()%100;

    // ***Insering nodes***
    // Arguments:
    // 1 - RB_BST_TREE* self:
    //      instance of the tree
    // 2 - int key:
    //      key value of a node
    // 3 - void* info:
    //      pointer to info data that will be stored, behavior depends
    //      the init tree second argument
    //  Return:
    //      true: if the node was inserted
    //      false: if don't, probably there are a node with this key
    rb_insert(myTree, key[0], &tst);

    for (i = 1; i < ELEMENTS; i++){
        key[i] = rand()%100;
        rb_insert(myTree, key[i], &tst);
        tst.tcb -= 2;
        tst.ticket -= 1;
    }
    printf("Insertion Order:");
    for (i = 0; i < ELEMENTS; i++){
        printf(" %i", key[i]);
    }
    printf(".\n");

    // ***Printing Tree***
    // Arguments:
    // 1 - RB_BST_TREE* self:
    //      instance of the tree
    rb_print_tree(myTree);


    // ***Searching nodes***
    // Arguments:
    // 1 - RB_BST_TREE* self:
    //      instance of the tree
    // 2 - int key:
    //      key value of the node that want to be found
    // Return:
    //      - void *info: if was found
    //      - NULL: if don't
    // BE CAREFUL: if you modify any field with this pointer it will imply
    //      that the info field on tree node will change too
    // To do local copies use 'memcpy(destination, source, size_in_bytes)'
    // EXAMPLE:
    //      memcpy(rb_search(TREE, KEY), local_copy, sizeof(STRUCT_INFO));
    aux = (TEST *)rb_search(myTree, key[0]);
    printf("KEY(%i) %s \n", key[0], aux != NULL?"found":"not found");
    printf("TCB: %i TICKET: %i\n", aux->tcb++, aux->ticket++);
    aux = (TEST *)rb_search(myTree, key[0]);
    printf("KEY(%i) %s \n", key[0], aux != NULL?"found":"not found");
    printf("TCB: %i TICKET: %i\n", aux->tcb, aux->ticket);

    // ***Delete node***
    // Arguments:
    // 1 - RB_BST_TREE* self:
    //      instance of the tree
    // 2 - int key:
    //      key value of the node that want to be found
    // Return:
    //      - true: if the node as deleted
    //      - false: if don't, probably the key wasn't in the tree
    rb_delete(myTree, key[ELEMENTS-1]);

    rb_print_tree(myTree);
    for (i = ELEMENTS-2; i >= 0; i--){
        rb_delete(myTree, key[i]);
        rb_print_tree(myTree);
        printf("MAX KEY: %d\n", rb_get_key_max(myTree));
    }

    // ***Destroy Tree***
    // Arguments:
    // 1 - RB_BST_TREE* self:
    //      instance of the tree
    // Free all pointers that was created by the module
    rb_destroy_tree(myTree);
    return 0;
}
